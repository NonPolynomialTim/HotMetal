// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Weapons/Personal/PersonalWeapon.h"
#include "BaseCharacter.generated.h"

class UInputComponent;
class UCameraComponent;
class USkeletalMeshComponent;
class UCurveFloat;
class UTimelineComponent;
class UAimingComponent;
class UHealthComponent;
class USoundBase;
class UAnimMontage;
class AWheeledVehicleBase;
class UBoardableComponent;

UENUM(BlueprintType)
enum class EActionState : uint8
{
	IDLE				UMETA(DisplayName = "Idle"),
	RELOADING			UMETA(DisplayName = "Reloading"),
	SWITCHING_WEAPONS	UMETA(DisplayName = "Switching Weapons")
};

UCLASS(config = Game)
class HOTMETAL_API ABaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ABaseCharacter(const FObjectInitializer& ObjectInitializer);

	/****************************************************
	* FUNCTIONS											*
	*****************************************************/
	/** Returns Mesh1P subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetMesh1P() const { return Mesh1P; }
	/** Returns Mesh3P subobject **/
	FORCEINLINE class USkeletalMeshComponent* GetMesh3P() const { return Mesh3P; }
	/** Returns FirstPersonCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetFirstPersonCameraComponent() const { return FirstPersonCameraComponent; }
	/** Returns AimingComponent subobject **/
	FORCEINLINE class UAimingComponent* GetAimingComponent() const { return AimingComp; }

	void ResetPlayerInputComponent();

	UFUNCTION()
	void PlayerDamaged();

	UFUNCTION(BlueprintImplementableEvent)
	void OnPlayerDamaged();

	UFUNCTION(BlueprintImplementableEvent)
	void OnHitEnemy();

	UFUNCTION()
	void PlayerDied();

	UFUNCTION(BlueprintImplementableEvent)
	void OnPlayerDied();

	void Turn(float Val);

	void LookUp(float Val);

	void ClearLookInput();

	void SetCurrentVehicle(AActor* Vehicle);

	AActor* GetCurrentVehicle();

	void SetCurrentBoardableComponent(UBoardableComponent* Boardable);

	UBoardableComponent* GetCurrentBoardableComponent();

	UFUNCTION(BlueprintCallable)
	APersonalWeapon* GetPrimaryWeapon();

	bool GetIsADS();

	void SetIsADS(bool IsADS);

	UFUNCTION(BlueprintCallable)
	FVector_NetQuantize100 GetLookRotation();

	void OnStartCrouch(float HeightAdjust, float ScaledHeightAdjust) override;

	void OnEndCrouch(float HalfHeightAdjust, float ScaledHalfHeightAdjust) override;

	UFUNCTION()
	void CrouchTimelineUpdate(float Value);

	UFUNCTION(BlueprintCallable)
	void SetIsCrouched(bool IsCrouched);

	bool IsPassenger();

	bool IsOperator();

	bool IsInVehicle();

	/****************************************************
	* PROPERTIES										*
	*****************************************************/
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	/** Gun muzzle's offset from the characters location */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		FVector GunOffset;

	/* This is when calculating the trace to determine what the weapon has hit */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		float WeaponRange;

	/* This is multiplied by the direction vector when the weapon trace hits something to apply velocity to the component that is hit */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		float WeaponDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		float InteractionRange;

protected:
	/****************************************************
	* FUNCTIONS											*
	*****************************************************/
	void BeginPlay();

	void Tick(float DeltaTime) override;

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerPlayerDied();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastPlayerDied();

	void DestroyCharacter();

	void CheckForInteractableComponents();

	FHitResult GetFirstInteractableInReach();

	void GetRaycastEndPoints(FVector &OutRaycastStart, FVector &OutRaycastEnd);

	/** Handler for a touch input beginning. */
	void TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location);

	void Jump();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerJump();

	UFUNCTION(NetMulticast, Reliable)
	void MulticastJump();

	void StartCrouch();

	void StopCrouch();

	void OnPressTrigger();

	void OnReleaseTrigger();

	void OnReload();

	void OnADS();

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles strafing movement, left and right */
	void MoveRight(float Val);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Called via input to move a controlled WheeledVehicleBase forward. */
	void WheeledVehicleForward(float Val);

	/** Called via input to move a controlled WheeledVehicleBase left and right. */
	void WheeledVehicleRight(float Val);

	void WheeledVehicleStartHandbrake();

	void WheeledVehicleStopHandbrake();

	/*
	 * Performs a trace between two points
	 *
	 * @param	StartTrace	Trace starting point
	 * @param	EndTrac		Trace end point
	 * @returns FHitResult returns a struct containing trace result - who/what the trace hit etc.
	 */
	FHitResult WeaponTrace(const FVector& StartTrace, const FVector& EndTrace) const;

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

	/** Structure that handles touch data so we can process the various stages of touch. */
	struct TouchData
	{
		TouchData() { bIsPressed = false; Location = FVector::ZeroVector; }
		bool bIsPressed;
		ETouchIndex::Type FingerIndex;
		FVector Location;
		bool bMoved;
	};

	/*
	 * Handle begin touch event.
	 * Stores the index and location of the touch in a structure
	 *
	 * @param	FingerIndex	The touch index
	 * @param	Location	Location of the touch
	 */
	void BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location);

	/*
	 * Handle end touch event.
	 * If there was no movement processed this will fire a projectile, otherwise this will reset pressed flag in the touch structure
	 *
	 * @param	FingerIndex	The touch index
	 * @param	Location	Location of the touch
	 */
	void EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location);

	/*
	 * Handle touch update.
	 * This will update the look position based on the change in touching position
	 *
	 * @param	FingerIndex	The touch index
	 * @param	Location	Location of the touch
	 */
	void TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location);

	// Structure to handle touch updating
	TouchData	TouchItem;

	/*
	 * Configures input for touchscreen devices if there is a valid touch interface for doing so
	 *
	 * @param	InputComponent	The input component pointer to bind controls to
	 * @returns true if touch controls were enabled.
	 */
	void TryEnableTouchscreenMovement(UInputComponent* InputComponent);

	void Interact();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerSpawnDefaultWeapons();

	void ActivateWeapon(APersonalWeapon* NewWeapon);

	void ActivatePrimaryWeapon();

	void ActivateSecondaryWeapon();

	void ActivateFists();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerNotifySwapWeapon(APersonalWeapon* NewWeapon);

	UFUNCTION(NetMulticast, Reliable)
	void MulticastNotifySwapWeapon(APersonalWeapon* NewWeapon);

	UFUNCTION()
	void HideWeapon(APersonalWeapon* Weapon);

	UFUNCTION()
	void UnhideWeapon(APersonalWeapon* Weapon);

	UFUNCTION()
	void UnholsterWeapon(APersonalWeapon* Weapon);

	void OnWeaponReady();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerInteract(UObject* Interactable);

	UFUNCTION(BlueprintImplementableEvent)
	void OnActiveWeaponChanged();

	/****************************************************
	* COMPONENTS										*
	*****************************************************/
	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Mesh)
	USkeletalMeshComponent* Mesh1P;

	/** Pawn mesh: 3rd person view (body; seen by everyone but self) */
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = Mesh)
	USkeletalMeshComponent* Mesh3P;

	/** First person camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FirstPersonCameraComponent;
	
	UPROPERTY()
	UTimelineComponent* CrouchTimeline;

	UPROPERTY(EditAnywhere)
	UCurveFloat* CrouchCurve;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UAimingComponent* AimingComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Gameplay")
	UHealthComponent* HealthComp;

	/****************************************************
	* PROPERTIES										*
	*****************************************************/
	UPROPERTY(Replicated)
	UObject* InteractableObject;

	bool bTraceInteractable = true;

	UPROPERTY(BlueprintReadOnly)
	AActor* CurrentVehicle;

	UPROPERTY(BlueprintReadOnly)
	UBoardableComponent* CurrentBoardableComponent;

	UPROPERTY(EditDefaultsOnly, Category = "Gameplay")
	TSubclassOf<APersonalWeapon> DefaultPrimaryWeaponClass;

	UPROPERTY(EditDefaultsOnly, Category = "Gameplay")
	TSubclassOf<APersonalWeapon> DefaultSecondaryWeaponClass;

	UPROPERTY(EditDefaultsOnly, Category = "Gameplay")
	TSubclassOf<APersonalWeapon> FistClass;

	FTimerHandle SwitchWeaponTimer;

	UPROPERTY(ReplicatedUsing = OnRep_ActiveWeapon, BlueprintReadOnly)
	APersonalWeapon* ActiveWeapon;

	UPROPERTY(Replicated, BlueprintReadOnly)
	APersonalWeapon* PrimaryWeapon;

	UPROPERTY(Replicated, BlueprintReadOnly)
	APersonalWeapon* SecondaryWeapon;

	UPROPERTY(BlueprintReadOnly)
	APersonalWeapon* Fists;

	UPROPERTY(Replicated, VisibleAnywhere, BlueprintReadOnly)
	bool IsADS;

	UPROPERTY(BlueprintReadWrite)
		float StandingStartingHalfHeight;

	UPROPERTY(BlueprintReadWrite)
		float PreviousHalfHeight;

	UPROPERTY(BlueprintReadWrite)
		FVector Mesh3PRelativeStartingLocation;

	UPROPERTY(BlueprintReadOnly)
		bool JustJumped = false;

	EActionState ActionState = EActionState::IDLE;

private:

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerUpdateLookRotation(FVector_NetQuantize100 LookRotation);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerUpdateIsADS(bool NewIsADS);

	UFUNCTION()
	void OnRep_ActiveWeapon();

	UPROPERTY(Replicated)
	FVector_NetQuantize100 LookRotation;
};
