// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectileBase.h"
#include "Kismet/GameplayStatics.h"
#include "Components/StaticMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AProjectileBase::AProjectileBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>("StaticMeshComponent");
	RootComponent = StaticMeshComp;
	ParticleSystemComp = CreateDefaultSubobject<UParticleSystemComponent>("ParticleSystemComponent");
	ParticleSystemComp->SetupAttachment(RootComponent);
	ParticleSystemComp->bAutoActivate = false;
}

// Called when the game starts or when spawned
void AProjectileBase::BeginPlay()
{
	Super::BeginPlay();
	
	if (!StaticMeshComp)
	{
		UE_LOG(LogTemp, Error, TEXT("No StaticMeshComponent assigned on %s!"), *(this->GetName()));
		return;
	}

	if (!ParticleSystemComp)
	{
		UE_LOG(LogTemp, Error, TEXT("No ParticleSystemComponent assigned on %s!"), *(this->GetName()));
		return;
	}

	if (Role == ROLE_Authority)
	{
		SetReplicates(true);
		SetReplicateMovement(true);
		StaticMeshComp->OnComponentHit.AddDynamic(this, &AProjectileBase::OnHit);
	}
}

// Called every frame
void AProjectileBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AProjectileBase::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!DamageType)
	{
		UE_LOG(LogTemp, Error, TEXT("No DamageType assigned on %s!"), *(GetName()));
		return;
	}

	if (Role == ROLE_Authority)
	{
		if (!StaticMeshComp)
		{
			UE_LOG(LogTemp, Error, TEXT("No StaticMeshComponent assigned on %s!"), *(this->GetName()));
			return;
		}

		if (!ParticleSystemComp)
		{
			UE_LOG(LogTemp, Error, TEXT("No ParticleSystemComponent assigned on %s"), *(this->GetName()));
			return;
		}

		StaticMeshComp->SetVisibility(false);
		StaticMeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		ParticleSystemComp->Activate();

		UGameplayStatics::ApplyRadialDamage(
			this,
			50.0f,
			Hit.Location,
			100.0f,
			DamageType,
			TArray<AActor*>()
		);

		ImpactLocation = Hit.Location;
	}
}

void AProjectileBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AProjectileBase, ImpactLocation);
}

void AProjectileBase::OnRep_Impact()
{
	if (!StaticMeshComp)
	{
		UE_LOG(LogTemp, Error, TEXT("No StaticMeshComponent assigned on %s!"), *(this->GetName()));
		return;
	}

	if (!ParticleSystemComp)
	{
		UE_LOG(LogTemp, Error, TEXT("No ParticleSystemComponent assigned on %s"), *(this->GetName()));
		return;
	}

	StaticMeshComp->SetVisibility(false);
	StaticMeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	ParticleSystemComp->Activate();
}