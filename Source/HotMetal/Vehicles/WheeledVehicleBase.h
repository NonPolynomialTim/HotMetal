// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WheeledVehicle.h"
#include "WheeledVehicleBase.generated.h"

class UBaseVehicleComponent;
class UOperatorComponent;
class UHealthComponent;
class AProjectileBase;
class ABaseCharacter;

/**
 * 
 */
UCLASS()
class HOTMETAL_API AWheeledVehicleBase : public AWheeledVehicle
{
	GENERATED_BODY()
public:
	/****************************************************
	* FUNCTIONS											*
	*****************************************************/
	AWheeledVehicleBase(const class FObjectInitializer& PCIP);

	void OnThrottleInput(float ThrottleInput);

	void OnSteeringInput(float SteeringInput);

	void LookUp(float Val);

	void Turn(float Val);

	void StartHandbrake();

	void StopHandbrake();

	UFUNCTION()
	void SetController(AController* NewController);

	UFUNCTION()
	void OnControlled();

	UFUNCTION()
	void OnUncontrolled();

	/****************************************************
	* COMPONENTS										*
	*****************************************************/
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Gameplay")
	UBaseVehicleComponent* VehicleComp;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Gameplay")
	UOperatorComponent* OperatorComp;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Gameplay")
	UHealthComponent* HealthComp;

protected:
	virtual void BeginPlay();
};
