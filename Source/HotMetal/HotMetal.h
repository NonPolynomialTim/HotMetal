// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#define COLLISION_WEAPON		ECC_GameTraceChannel2
#define COLLISION_INTERACT		ECC_GameTraceChannel3
#define NO_STENCIL				0
#define OUTLINE_STENCIL			252