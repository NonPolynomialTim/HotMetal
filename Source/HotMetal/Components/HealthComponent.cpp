// Fill out your copyright notice in the Description page of Project Settings.

#include "HealthComponent.h"
#include "GameFramework/Actor.h"
#include "GameFramework/Character.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/WidgetComponent.h"
#include "Net/UnrealNetwork.h"

// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	SetIsReplicated(true);

	Owner = GetOwner();
}

float UHealthComponent::GetHealthPercentage() const
{
	return ((float)CurrentHealth / (float)MaxHealth);
}

float UHealthComponent::GetDestroyDelay()
{
	return DestroyDelay;
}

// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	if (!Owner)
	{
		UE_LOG(LogTemp, Error, TEXT("%s does not have an owner assigned!"), *(GetName()));
	}

	if (GetOwnerRole() == ROLE_Authority)
	{
		if (Owner)
		{
			Owner->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::TakeAnyDamage);
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("%s does not have an owner assigned!"), *(GetName()));
		}

		CurrentHealth = MaxHealth;
	}
}

void UHealthComponent::TakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (GetOwnerRole() == ROLE_Authority)
	{
		CurrentHealth -= Damage;

		if (CurrentHealth <= 0 && !bIsDead)
		{
			CurrentHealth = 0;
			OwnerDied();
		}
	}
}

void UHealthComponent::OwnerDied()
{
	bIsDead = true;

	OnOwnerDied.Broadcast();
}

void UHealthComponent::OnRep_CurrentHealth()
{
	OnOwnerDamaged.Broadcast();
}

void UHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UHealthComponent, CurrentHealth);
	DOREPLIFETIME(UHealthComponent, bIsDead);
}