// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/PassengerComponent.h"
#include "OperatorComponent.generated.h"

class UBaseVehicleComponent;

UCLASS( meta=(BlueprintSpawnableComponent) )
class HOTMETAL_API UOperatorComponent : public UPassengerComponent
{
	GENERATED_BODY()
protected:
	void BeginPlay() override;

	void GetIn(ABaseCharacter* InstigatorCharacter) override;

	void GetOut(ABaseCharacter* InstigatorCharacter) override;

private:
	void MulticastOnEnter_Implementation(ABaseCharacter* LocalOperator) override;

	void MulticastOnExit_Implementation(ABaseCharacter* LocalOperator) override;

	AActor* OwnerVehicleActor;

	UBaseVehicleComponent* OwnerVehicleComponent;
};
