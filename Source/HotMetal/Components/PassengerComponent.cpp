// Fill out your copyright notice in the Description page of Project Settings.

#include "PassengerComponent.h"
#include "Characters/BaseCharacter.h"
#include "GameFramework/PawnMovementComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Controller.h"

void UPassengerComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (Operator && (Operator->Role == ROLE_SimulatedProxy))
	{
		FRotator NewRotation = FRotator(0.0f, Operator->GetLookRotation().Rotation().Yaw, 0.0f);
		Operator->SetActorRotation(NewRotation);
	}
}

void UPassengerComponent::OnInteract(AActor* Instigator)
{
	if (!Instigator)
	{
		UE_LOG(LogTemp, Error, TEXT("OnInteract called on %s with null Instigator!"), *(GetName()));
		return;
	}

	ABaseCharacter* InstigatorCharacter = Cast<ABaseCharacter>(Instigator);
	if (!InstigatorCharacter)
	{
		UE_LOG(LogTemp, Error, TEXT("Instigator on %s is not of type ABaseCharacter!"), *(GetName()));
		return;
	}

	if (!GetOperator()) // Get in
	{
		GetIn(InstigatorCharacter);
	}
	else if (GetOperator() == Instigator) // Get out
	{
		GetOut(InstigatorCharacter);
	}
}

void UPassengerComponent::GetIn(ABaseCharacter* InstigatorCharacter)
{
	if (!GetOwnerMesh())
	{
		UE_LOG(LogTemp, Error, TEXT("RootComponent of %s's owner is not of type USkeletalMesh!"), *(GetName()));
		return;
	}

	SetOperator(InstigatorCharacter);
	InstigatorCharacter->SetCurrentBoardableComponent(this);
	InstigatorCharacter->SetIsADS(false);
	InstigatorCharacter->bUseControllerRotationYaw = false;
	InstigatorCharacter->GetFirstPersonCameraComponent()->bUsePawnControlRotation = false;
	GetOperator()->GetCharacterMovement()->Deactivate();
	GetOperator()->GetCharacterMovement()->GravityScale = 0.0f;
	GetOperator()->GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetOperator()->AttachToComponent(GetOwnerMesh(), FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), GetBoardableSocketName());
	MulticastOnEnter(GetOperator());
}

void UPassengerComponent::GetOut(ABaseCharacter* InstigatorCharacter)
{
	GetOperator()->DetachFromActor(FDetachmentTransformRules(EDetachmentRule::KeepWorld, EDetachmentRule::KeepRelative, EDetachmentRule::KeepWorld, true));
	GetOperator()->GetCharacterMovement()->Activate();
	GetOperator()->GetCharacterMovement()->GravityScale = 1.0f;
	GetOperator()->SetActorLocation(
		GetComponentLocation() +
		GetExitForwardComponent() +
		GetExitRightComponent() +
		GetExitUpComponent());
	GetOperator()->GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	GetOperator()->GetController()->Possess(GetOperator());
	MulticastOnExit(GetOperator());
	InstigatorCharacter->GetFirstPersonCameraComponent()->bUsePawnControlRotation = true;
	InstigatorCharacter->bUseControllerRotationYaw = true;
	InstigatorCharacter->SetCurrentBoardableComponent(nullptr);
	SetOperator(nullptr);
}

void UPassengerComponent::MulticastOnEnter_Implementation(ABaseCharacter* LocalOperator)
{
	LocalOperator->GetFirstPersonCameraComponent()->SetRelativeRotation(FRotator(0.0f, 0.0f, 0.0f));
	LocalOperator->SetCurrentBoardableComponent(this);
	LocalOperator->SetIsADS(false);
	LocalOperator->GetCharacterMovement()->Deactivate();
	LocalOperator->GetCharacterMovement()->GravityScale = 0.0f;
	LocalOperator->GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	LocalOperator->bUseControllerRotationYaw = false;
	LocalOperator->GetFirstPersonCameraComponent()->bUsePawnControlRotation = false;
}

void UPassengerComponent::MulticastOnExit_Implementation(ABaseCharacter* LocalOperator)
{
	if (!GetOwner())
	{
		UE_LOG(LogTemp, Error, TEXT("PassengerComponent does not have a valid Owner!"));
		return;
	}

	if (GetOwnerRole() != ROLE_Authority)
	{
		if (LocalOperator->IsLocallyControlled())
		{
			AController* OperatorController = LocalOperator->GetController();

			if (!OperatorController)
			{
				UE_LOG(LogTemp, Error, TEXT("LocalOperator does not have a valid controller!"));
				return;
			}

			FRotator PreExitRotation = LocalOperator->GetFirstPersonCameraComponent()->GetComponentRotation();
			LocalOperator->SetActorRotation(FRotator(0.0f, 0.0f, 0.0f));
			OperatorController->SetControlRotation(PreExitRotation.Vector().Rotation());
		}

		LocalOperator->GetFirstPersonCameraComponent()->bUsePawnControlRotation = true;
		LocalOperator->bUseControllerRotationYaw = true;
		LocalOperator->GetCharacterMovement()->Activate();
		LocalOperator->GetCharacterMovement()->GravityScale = 1.0f;
		LocalOperator->SetActorLocation(
			GetComponentLocation() +
			GetExitForwardComponent() +
			GetExitRightComponent() +
			GetExitUpComponent());

		//LocalOperator->SetActorRotation(FRotator(0.0f, 0.0f, 0.0f));


		LocalOperator->GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		LocalOperator->SetCurrentBoardableComponent(nullptr);
	}
}
