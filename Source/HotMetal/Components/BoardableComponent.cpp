// Fill out your copyright notice in the Description page of Project Settings.

#include "BoardableComponent.h"
#include "Characters/BaseCharacter.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "HotMetal.h"
#include "Net/UnrealNetwork.h"

UBoardableComponent::UBoardableComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	SetIsReplicated(true);
	SetRenderCustomDepth(true);
	SetCollisionProfileName(FName("VehicleParts"));
}

void UBoardableComponent::BeginPlay()
{
	Super::BeginPlay();

	OwnerMesh = Cast<USkeletalMeshComponent>(GetOwner()->GetRootComponent());

	if (!OwnerMesh)
	{
		UE_LOG(LogTemp, Error, TEXT("RootComponent of %s's owner is not of type USkeletalMesh!"), *(GetName()));
		return;
	}
}

FVector UBoardableComponent::GetExitForwardComponent()
{
	return GetForwardVector() * RelativeExitLocationForward;
}

FVector UBoardableComponent::GetExitRightComponent()
{
	return GetRightVector() * RelativeExitLocationRight;
}

FVector UBoardableComponent::GetExitUpComponent()
{
	return GetUpVector() * RelativeExitLocationUp;
}

void UBoardableComponent::StartFocus()
{
	SetCustomDepthStencilValue(OUTLINE_STENCIL);
}

void UBoardableComponent::StopFocus()
{
	SetCustomDepthStencilValue(NO_STENCIL);
}

void UBoardableComponent::OnInteract(AActor* Instigator)
{

}

FName UBoardableComponent::GetBoardableSocketName()
{
	return BoardableSocketName;
}

void UBoardableComponent::SetBoardableSocketName(FName BoardableSocketName)
{
	this->BoardableSocketName = BoardableSocketName;
}

ABaseCharacter* UBoardableComponent::GetOperator()
{
	return Operator;
}

void UBoardableComponent::SetOperator(ABaseCharacter* Operator)
{
	this->Operator = Operator;
}

USkeletalMeshComponent* UBoardableComponent::GetOwnerMesh()
{
	return OwnerMesh;
}

void UBoardableComponent::SetOwnerMesh(USkeletalMeshComponent* OwnerMesh)
{
	this->OwnerMesh = OwnerMesh;
}

#pragma region Replication

void UBoardableComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UBoardableComponent, Operator);
}

#pragma endregion