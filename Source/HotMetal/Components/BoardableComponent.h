// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "Interfaces/Interactable.h"
#include "BoardableComponent.generated.h"

class ABaseCharacter;

/**
 * 
 */
UCLASS( Abstract )
class HOTMETAL_API UBoardableComponent : public UStaticMeshComponent, public IInteractable
{
	GENERATED_BODY()
	
public:
	UBoardableComponent();

	void StartFocus() override;

	void StopFocus() override;

	virtual void OnInteract(AActor* Instigator);

	FName GetBoardableSocketName();

	void SetBoardableSocketName(FName BoardableSocketName);
	
	ABaseCharacter* GetOperator();

	void SetOperator(ABaseCharacter* Operator);

	USkeletalMeshComponent* GetOwnerMesh();

	void SetOwnerMesh(USkeletalMeshComponent* OwnerMesh);

protected:
	void BeginPlay();

	FVector GetExitForwardComponent();

	FVector GetExitRightComponent();

	FVector GetExitUpComponent();
	
	UPROPERTY(EditDefaultsOnly)
	float RelativeExitLocationForward;

	UPROPERTY(EditDefaultsOnly)
	float RelativeExitLocationRight;

	UPROPERTY(EditDefaultsOnly)
	float RelativeExitLocationUp;

	UPROPERTY(Replicated, BlueprintReadOnly)
	ABaseCharacter* Operator;

private:
	UPROPERTY(EditDefaultsOnly)
	FName BoardableSocketName;

	USkeletalMeshComponent* OwnerMesh;
};
