// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "AimingComponent.generated.h"

class ABaseCharacter;
class APersonalWeapon;
class UCameraComponent;
class UOperatorComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class HOTMETAL_API UAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UAimingComponent();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	FVector GetFiringVector();

	void ApplyRecoil();

	UFUNCTION(BlueprintCallable)
	bool GetIsADS();
	
	UFUNCTION(BlueprintCallable)
	bool ShouldShowCrosshairs();

	UFUNCTION(BlueprintCallable)
	float GetCurrentSpread();

	void SetOwningCharacter(ABaseCharacter* OwningCharacter);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

private:
	FRotator CalculateRecoilRotator();

	ABaseCharacter* OwningCharacter;

	UCameraComponent* CameraComp;

	float CurrentVerticalRecoil;

	float CurrentHorizontalRecoil;

	float CurrentSpread;

	float CurrentHorizontalDecreasePerSecond;

	float CurrentVerticalDecreasePerSecond;

	float CurrentSpreadDecreasePerSecond;

	float LastShotTime;

	FRotator PreviousRecoil = FRotator(0.0f, 0.0f, 0.0f);

	FRotator CurrentRecoil = FRotator(0.0f, 0.0f, 0.0f);

	//bool IsADS;
};
